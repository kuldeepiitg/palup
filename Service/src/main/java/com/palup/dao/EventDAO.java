package com.palup.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * @author  RKS
 * @since   19-sep-15
 */
public interface EventDAO {

    @SqlUpdate("insert into event (ownerId, title, description, latitude, longitude, date) " +
            "values (:ownerId, :title, :description, :latitude, :longitude, :date)")
    int saveEvent(@Bind("ownerId") String ownerId, @Bind("title") String title, @Bind("description") String description,
                  @Bind("latitude") String latitude, @Bind("longitude") String longitude, @Bind("date") String date);

}
