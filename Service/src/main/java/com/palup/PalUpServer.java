package com.palup;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.palup.resources.PalUpResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.skife.jdbi.v2.DBI;

/**
 * @author  RKS
 * @since   13-sep-15.
 */

public class PalUpServer extends Application<PalUpConfiguration>{


    private Injector injector;

    public static void main(String[] args) throws Exception {
        new PalUpServer().run(args);
    }

    @Override
    public String getName() {
        return getConfigurationClass().getName();
    }

    @Override
    public void initialize(Bootstrap<PalUpConfiguration> bootstrap) {
        //do nothing for now
    }

    @Override
    public void run(PalUpConfiguration configuration, Environment environment) throws Exception {
        final DBI database = new DBIFactory().build(environment, configuration.getDatabase(), "database");

        final PalUpModule palUpModule = new PalUpModule(configuration, environment, database);
        injector = Guice.createInjector(palUpModule);

        // Register the resources
        environment.jersey().register(injector.getInstance(PalUpResource.class));

        environment.jersey().register(injector.getInstance(MultiPartFeature.class));

    }
}
