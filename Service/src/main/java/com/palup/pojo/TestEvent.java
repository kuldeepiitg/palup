package com.palup.pojo;

/**
 * Created by rahul on 9/19/15.
 */
public class TestEvent {
    private String name;
    private int id;

    public TestEvent(){}

    public TestEvent(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
